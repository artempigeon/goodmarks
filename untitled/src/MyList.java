import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyList {
    private List<List<Integer>> lists;

    MyList() {
        this.lists = new ArrayList<>();
    }

    public int length() {
        return lists.size();
    }

    void print() {
        for (List<Integer> list : lists) {
            for (int item : list) {
                System.out.print(item + " ");
            }
            System.out.println();
        }
    }

    void initRandom(int max, int length) {
        Random rand = new Random();
        lists.clear();
        for (int i = 0; i < length; i++) {
            lists.add(new ArrayList());
            for (int j = 0; j < length; j++) {
                lists.get(i).add(rand.nextInt(max + 1));
            }
        }
    }

    public int min() {
        int min = lists.get(0).get(0);
        for (List<Integer> list : lists) {
            for (int item : list) if (min > item) min = item;
        }
        return min;
    }

    public int max() {
        int max = lists.get(0).get(0);
        for (List<Integer> list : lists) {
            for (int item : list) if (max < item) max = item;
        }
        return max;
    }


    public int minApi() {
        int min = lists.get(0).get(0);
        for (List<Integer> list : lists) {
            int lMin = list.stream().min(Integer::compareTo).get();
            if (min > lMin) min = lMin;
        }
        return min;
    }

    public int maxApi() {
        int max = lists.get(0).get(0);
        for (List<Integer> list : lists) {
            int lMax = list.stream().max(Integer::compareTo).get();
            if (max < lMax) max = lMax;
        }
        return max;
    }

}
